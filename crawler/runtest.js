// load modules
var htmlparser = require("htmlparser");
var url = require("url");
var http = require("http");
var redis = require("redis");

// declare url, path
var clienUrl = "www.clien.net";
var boardPath = "/cs2/bbs/board.php?bo_table=";
var boardType = ['news','use','lecture','jirum','park','kin','sold'];
var boardPage = {"park":3}
//var boardType = ['news','use','lecture','jirum','useful','park','kin','sold'];

// declare regexp
var boardRe = /<tr class="mytr">[\s\S]+?<\/tr>/gmi;
var boardRe_jirum = /<tr onmouseover="this.style.backgroundColor='#f3f5f8'" [\s\S]+?<\/tr>/gmi;
var mytrSubject = /<td class="post_subject">[\s\S]+?<\/td>/im;
var bTypeRe = /[?&]bo_table=([^&]+)&?/;
var boardIdxRe = /<td>(\d+)<\/td>\s*<td class="post_(subject|category)">/im;
var rowDateRe = /title="(\d{4}-\d{2}-\d{2}\s+\d{2}:\d{2}:\d{2})"/;

// variables
var expireTime = 86400 * 2; // expire 2 day after

// configure redis client
var rClient = redis.createClient();
rClient.on("error",function(err){
	console.log("Error redis : " + err );
});

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

// configure htmlparser
var handler = new htmlparser.DefaultHandler(function(error, dom){
	if(!error && dom[0]){
		var tdCount = 0;
		var row = {};
		//var row = {type:bType, idx:'',title:'',href:'',reps:'',hit:'',date:'',writer:'',cate:''};
		// scan each elements of a row
		try {
			for(var td in dom[0].children) {
				var el = dom[0].children[td];
				if( el.data == 'td' && tdCount == 0) {
					if(el.children[0]) row.idx = el.children[0].data;
					tdCount++;
				} else if (el.data == 'td' && tdCount == 1) {
					if(el.children[0] && el.children[0].attribs) {
						row.date = el.children[0].attribs.title;
					} else {
						row.date = '-';
					}
					tdCount++;
				} else if (el.data == 'td' && tdCount == 2) {
					if(el.children[0]) row.hit = el.children[0].data;
					tdCount++;
				} else if (el.name == 'btype' && el.attribs) {
					row.bd = el.attribs.val;
					row.ch = el.attribs.ch;
				} else if (el.attribs && el.attribs.class) {
					if (el.attribs.class == "post_category"){
						if (el.children[0] && el.children[0].children) {
							row.cate = el.children[0].children[0].data.replace(/[\[\]]/g,'');
						} else {
							row.cate = "-";
						}
					} else if (el.attribs.class == "post_subject") {	// title, href, reps
						if(el.children.length > 1) {
							for(var i in el.children) {
								if(el.children[i].name && el.children[i].name == 'a') {
									row.href = el.children[i].attribs.href.replace(/[?&]page=\d/g,'');
									row.title = el.children[i].children[0].data;
								} else if (el.children[i].name && el.children[i].name == 'span') {
									row.reps = el.children[i].children[0].data.replace(/[\[\]]/g,'');
								}
							} 
						} else if (el.children.length <= 1 && el.children[0].name == 'span'){
							row.href = "-";
							row.reps = "-";
							row.title = "삭제된 글 입니다.";
							if(el.children[0].children && el.children[0].children[0].type == 'text') {
								row.title = el.children[0].children[0].data;
							}
						}
					} else if (el.attribs.class == "post_name") {
						if(el.children[0].name && el.children[0].name == 'span') {
							row.writer = el.children[0].children[0].data;
						} else if (el.children[0].name && el.children[0].name == 'img') {
							row.writer = '<'+el.children[0].data.replace(/\salign='absmiddle' border='0'/,'')+'>';
							//row.writer = row.writer.replace(/\.\.\/data\/member\//,'');
						} else {
							row.writer = "-";
						}
					}
				}
			}			
			setDataTitle(row);
		} catch (err) {
			console.log(err.message);
			//console.log(dom[0]);
		}
	}
}, { ignoreWhitespace: true });


var setData = function(key, field, val) {
	rClient.hset(key, field, val, function() {
		rClient.expire(key, expireTime); // expire after a day
	});
}

var setDataTitle = function(row) {

	var fieldType = 'title';

	if(row instanceof Object) {
		if(row.idx && row.bd ) {
			var key = row.bd + "_" + row.idx;
			rClient.exists(key, function(err,orgData){
				if(orgData)	{	// exist data
					// compare data value
					rClient.hget(key, fieldType, function(err, orgData) {
						var oRet = JSON.parse(orgData);
						var updateFields = {};
						for(var k in row) {
							if(k == 'idx' || k == 'bd') { updateFields[k] = row[k]; continue; }
							if(!oRet[k]) { updateFields[k] = row[k]; continue; }
							if(row[k] != oRet[k]) { updateFields[k] = row[k]; }
						}
						if(Object.size(updateFields) > 2) {
							setData(key, fieldType, JSON.stringify(row));
							//console.log("update : " + JSON.stringify(updateFields));
							var msg = {
								"channels" : [row.ch]
								, "data" : {
									"t" : "u"
									,"p" : updateFields
								}
							};
							//
							// send update Broadcast with socket.io
							rClient.publish("juggernaut",JSON.stringify(msg));
						} else {
							// reset ttl
							rClient.expire(key, expireTime);
						}
					});
				} else {	// new data
					setData(key, fieldType, JSON.stringify(row));
					setData(key, 'ut', Math.floor(new Date(row.date).getTime() / 1000)); // unixtime
					console.log("create : " + key);
					var msg = {
						"channels" : [row.ch]
						, "data" : {
							"t" : "c"
							,"p" : row
						}
					};
					//
					// send create Broadcast with socket.io
					rClient.publish("juggernaut",JSON.stringify(msg));
				}
			});
		}
	}	
}

//var crawler = function

var parser = new htmlparser.Parser(handler);

for(var j in boardType) {

	(function(btype){

		setInterval(function() {

			var arrPage = [];
			var pgEnd = 1;
			if(boardPage[btype]) pgEnd = boardPage[btype];

			for(var pgNum = 1 ; pgNum <= pgEnd ; pgNum++) {
				arrPage.push(pgNum);
			}

			var crawler = function(arrPage){
				var pgNum = arrPage.shift();
				console.log("---- start get board : " + btype + ", page : " + pgNum + " ----");

				http.get( 
					{ host : clienUrl, port : 80, path : boardPath + btype + '&page=' + pgNum }
					, function(res){

					res.setEncoding('utf8');
					var data = "";

					res.on('data', function(chunk){
						data = data + chunk;
					});

					res.on('end', function() {
						var rows = [];
						if(btype=='jirum') rows = data.match(boardRe_jirum);
						else rows = data.match(boardRe);

						console.log("---- end get board : " + btype + ", page : " + pgNum + " ----");

						var listBoardIdx = {};
						for(var i in rows) {
							var str = rows[i].replace(/[\n\t]/g,'').replace(/<\/tr>/,"<btype val='"+btype+"' ch='cliem'></btype></tr>");
							parser.parseComplete(str);
							var re1 = str.match(boardIdxRe);
							var re2 = str.match(rowDateRe);
							if(re1 && re1[1] && re2 && re2[1]) {
								listBoardIdx[re1[1]] = (new Date(re2[1])).getTime() / 1000;
							} 
						}

						// refresh board idx list
						rClient.set(btype + "_p" + pgNum, JSON.stringify(listBoardIdx));

						if(arrPage.length) {
							crawler(arrPage);
						}
					});

				}).on('error', function(e) {
					console.log("Got error with board : " + btype + ", page : " + pgNum + " --- " + e.message);
				}).setTimeout(4000,function(){
					console.log("connection timed out!!");
					var msg = {
						"channels" : ["cliem"]
						, "data" : { "t" : "msg"
									,"txt" : "클리앙 서버와의 연결이 지연되고 있습니다."
								}
							};
					rClient.publish("juggernaut",JSON.stringify(msg));
					this.destroy();
				});
			}
			
			crawler(arrPage);

		}, 20000 );

	})(boardType[j]);
}

var getRankCm = function() {
	var yqlPath = "/v1/public/yql?q=SELECT%20*%20FROM%20html%20%0AWHERE%20url%3D%22http%3A%2F%2Fwww.clien.net%2Fcs2%2Fcm_index.php%22%20%0AAND%20xpath%3D%22%2F%2Fdl%5B%40class%3D'cmRanking'%5D%2F%2Fdd%22&format=json&callback=";
	http.get( 
		{ host : "query.yahooapis.com", port : 80, path : yqlPath }
		, function(res){

			res.setEncoding('utf8');
			var data = "";

			res.on('data', function(chunk){
				data = data + chunk;
			});

			res.on('end', function() {
				var r = JSON.parse(data);
				var cmBoard = {};

				try {
					for(var i in r.query.results.dd){
						if(i>9) break;
						var href = r.query.results.dd[i].a.href;
						var re1 = href.match(bTypeRe);
						if(re1 && re1[1]) {
							cmBoard[re1[1]] = r.query.results.dd[i].a.content;
						}
					}
				} catch (err) {
					console.log("error for loop with cmTop20.");
				}

				rClient.set("cmTop20", JSON.stringify(cmBoard));
				console.log("Set cmTop20");
				console.log(JSON.stringify(cmBoard));
			});
			
		}
	).on('error', function(e) {
		console.log("Got error Query YQL : get Cm Ranking --- " + e.message);
	}).setTimeout(10000,function(){
		console.log("connection timed out with YQL!!");
		this.destroy();
	});

}

getRankCm();
setInterval(getRankCm,3600000);	// setInterval 1 hour

var crawlerCm = function(){
	
	rClient.get('cmTop20',function(err,res){
		var boardTypeCm = JSON.parse(res);
		
		for(var j in boardTypeCm){

			(function(btype){
				var pgNum = 1;
				console.log("---- start get cm board : " + btype + ", page : " + pgNum + " ----");
				http.get(
						{ host : clienUrl, port : 80, path : boardPath + btype + '&page=' + pgNum }
						, function(res){

						res.setEncoding('utf8');
						var data = "";

						res.on('data', function(chunk){
							data = data + chunk;
						});

						res.on('end', function() {
							var rows = [];
							rows = data.match(boardRe);

							console.log("---- end get cm board : " + btype + ", page : " + pgNum + " ----");

							var listBoardIdx = {};
							for(var i in rows) {
								var str = rows[i].replace(/[\n\t]/g,'').replace(/<\/tr>/,"<btype val='"+btype+"' ch='cliem_cm'></btype></tr>");
								parser.parseComplete(str);
								var re1 = str.match(boardIdxRe);
								var re2 = str.match(rowDateRe);
								if(re1 && re1[1] && re2 && re2[1]) {
									listBoardIdx[re1[1]] = (new Date(re2[1])).getTime() / 1000;
								} 
							}

							// refresh board idx list
							rClient.set(btype + "_cmp" + pgNum, JSON.stringify(listBoardIdx));

						});

					}).on('error', function(e) {
						console.log("Got error with cm board : " + btype + ", page : " + pgNum + " --- " + e.message);
					}).setTimeout(4000,function(){
						console.log("connection timed out!!");
						var msg = {
							"channels" : ["cliem_cm"]
							, "data" : { "t" : "msg"
										,"txt" : "클리앙 서버와의 연결이 지연되고 있습니다."
									}
								};
						rClient.publish("juggernaut",JSON.stringify(msg));
						this.destroy();
					});
			})(j);
		}
	});
}

setInterval(crawlerCm, 33000);


var calTopParkPost = function() {

	var yesterdayUt = Math.floor(new Date().getTime()/1000) - 86400;

	rClient.keys('park_[^p]*',function(err, rep){
		//var hParkTop = {};
		var aParkTop = [];
		var arrKeys = rep;

		var traversing = function(arrKeys){

			var key = arrKeys.shift();

			if(key){
				rClient.hgetall(key, function(err, data){
					if(data && parseInt(data.ut)>=yesterdayUt){
						try {
							var post = JSON.parse(data.title);
							var hit = post.hit;
							if(hit.length>2){
								var countZero = 7 - hit.length;
								for( var i=0; i<countZero; i++ ) { hit = "0" + hit; }
								aParkTop.unshift(hit+"_"+post.bd+"_"+post.idx);
								aParkTop.sort();
								if( aParkTop.length > 20 ) { aParkTop.shift(); }
							}
						} catch(err) {
							console.log(err);
						}
					}

					if(arrKeys.length) traversing(arrKeys);
					else {
						rClient.set("todaysPark",JSON.stringify(aParkTop.reverse()));
						rClient.del("todaysParkDump");
						console.log(aParkTop);
					}
				});
			} 
		};

		traversing(arrKeys);

	});
}

calTopParkPost();
setInterval(calTopParkPost,300000); // 5 mins

//rClient.quit();
