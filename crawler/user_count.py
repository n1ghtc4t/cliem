#! -*- coding: utf-8 -*-
from juggernaut import Juggernaut

jug = Juggernaut()
listSess = {}

for event,data in jug.subscribe_listen():
    resp = {"t":"uc","msg":0}
    if data['channel'] and data['channel'] == 'cliem':
        if event == 'subscribe':
            print "subscribe : " + data['session_id']
            listSess[data['session_id']] = True
            userCount = len(listSess)
            resp['msg'] = userCount
            jug.publish('cliem',resp)
        elif event == 'unsubscribe':
            print "unsubscribe : " + data['session_id'] 
            try:
                del listSess[data['session_id']]
                userCount = len(listSess)
                resp['msg'] = userCount
                jug.publish('cliem',resp)
            except:
                pass

