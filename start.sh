#!/bin/bash

cd /root/work/cliem

tmux new-session -d -s cliem

tmux new-window -t cliem:1 -n 'juggernaut' '. ~/.bashrc && nvm use default && juggernaut'
tmux new-window -t cliem:2 -n 'crawler' '. ~/.bashrc && nvm run default /root/work/cliem/crawler/runtest.js'
tmux new-window -t cliem:3 -n 'web' '. ~/.bashrc && pythonbrew venv use cliem && python /root/work/cliem/dashboard/main.py'
tmux new-window -t cliem:4 -n 'usercount' '. ~/.bashrc && pythonbrew venv use cliem && python /root/work/cliem/crawler/user_count.py'
