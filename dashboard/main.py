#! -*- coding: utf-8 -*-
from flask import Flask
from flask import render_template, json, request
import redis, urllib, re

app = Flask(__name__)
boardType = ['news','use','lecture','jirum','useful','park','kin','sold']

@app.route("/")
def hello():
	arrTheme = {	'default':'http://c4t.so/assets/css/bootstrap.min.css',
					'superhero':'http://c4t.so/assets/css/bootstrap.superhero.min.css'
				}
	data = {'css':arrTheme['default']}

	theme = request.cookies.get('myTheme')
	if theme: data['css'] = arrTheme[theme]

	return render_template('hello.html',data=data)

@app.route("/getTodaysPark")
def getTodaysPark():

	rc = redis.StrictRedis(host='localhost', port=6379, db=0)
	
	todaysParkDump = rc.get('todaysParkDump')
	
	try:
		if not todaysParkDump:
			posts = []
			obj = json.loads(rc.get('todaysPark'))
			for key in obj:
				data = rc.hget(re.sub(r'^\d+_','',key), 'title')
				post = json.loads(data)
				posts.append({'idx':post['idx'], 'title':post['title'], 'writer':post['writer'], 'href':post['href']})
				#if len(posts) == 20: break
			todaysParkDump = json.dumps( posts )
			rc.set("todaysParkDump",todaysParkDump)
			rc.expire("todaysParkDump",300)
	except:
		print "error with todaysPark"

	#return json.dumps( posts )
	return todaysParkDump

@app.route("/getCmBoardType")
def getCmBoardType():
	rc = redis.StrictRedis(host='localhost', port=6379, db=0)

	cmType = rc.get('cmTop20')
	return cmType


@app.route("/getCurrent", methods=['GET','POST'])
def getCurrent():

	rc = redis.StrictRedis(host='localhost', port=6379, db=0)

	dash = {}

	arrBoard = []
	if request.form['bd']:
		obj = json.loads(request.form['bd'])
		if obj:
			for bd, t in obj.iteritems():
				if t: arrBoard.append(str(bd)+"_p1")
	else:
		arrBoard = rc.keys('*_p1')

	if request.form['bdcm']:
		obj = json.loads(request.form['bdcm'])
		if obj:
			for bd, t in obj.iteritems():
				if t: arrBoard.append(str(bd)+"_cmp1")

	#for bd in boardType:
	for bd in arrBoard:
		try:
			obj = json.loads(rc.get(bd))
		except:
			continue

		for post_idx, unixtime in obj.iteritems():
			dash_idx = int(unixtime)
			dash.setdefault(dash_idx,[])
			dash[dash_idx].append(re.sub(r'_[^_]*$','',bd)+"_"+str(post_idx))

	posts = []
	postsCount = 0

	for ut, items in sorted(dash.items(), key=lambda x:x[0], reverse=True):
		for i in items:
			if postsCount >= 50: return json.dumps(posts)
			data = rc.hget(i,"title")
			if data:
				obj = json.loads(data)
				obj['ut'] = ut
				posts.append(obj)
				postsCount += 1

	return json.dumps( posts )


@app.route("/noti", methods=['GET'])
def popupNoti():
	data = {'title': urllib.unquote(request.args.get('title','')),'link':urllib.unquote(request.args.get('link',''))}
	return render_template('popupNoti.html',data=data)

if __name__ == "__main__":
	app.run(debug=True,host="0.0.0.0")
