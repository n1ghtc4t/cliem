//var boardType = {"news":"새로운소식","use":"사용기 게시판","lecture":"강좌 게시판","jirum":"알뜰구매","useful":"유용한사이트","park":"모두의 공원","kin":"아무거나 질문","sold":"회원중고장터"};
var boardType = {"news":"새로운소식","use":"사용기 게시판","lecture":"강좌 게시판","jirum":"알뜰구매","park":"모두의 공원","kin":"아무거나 질문","sold":"회원중고장터"};
var postStr = "<div class='post' style='display:none;'><div class='post_info'><ul><li class='writer'></li><li class='board'></li><li class='cate'></li><li class='date'></li><li class='hit'><span class='badge badge-success' title='조회수'></span></li><li class='reps'><span class='badge badge-warning' title='댓글수'></span></li></ul></div><div class='post_title'></div></div>";
var feedLiStr = '<li style="float:left;width:150px;"><label class="checkbox"><input type="checkbox"></label></li>';
var indicator = ["＼","｜","／","―"];
var jug = new Juggernaut();

/* $('#btn_t').click(function(){
	var message = new Juggernaut.Message;
	message.type = "event";
	message.data = $('#input_t').val();
	jug.write(message);
}); */
var templatePost = $(postStr);
var pageTitle = document.title;
var reWords = [];
var feedBoard = {};
var feedBoardCm = {};

var prependPost = function(post){
	if(post.bd && (feedBoard[post.bd] || feedBoardCm[post.bd])) {
		var el = templatePost.clone();
		var href = post.href.replace(/^\.\./,"http://www.clien.net/cs2");
		if(post.title) el.find('div.post_title').html("<a href='"+href+"' class='title' target='_blank'>"+post.title+"</a>").append('<a href="#" title="toggle watch" class="btn_watch">☆</a>');
		if(post.reps) el.find('li.reps > span').text(post.reps);
		else el.find('li.reps > span').hide();
		if(post.hit) el.find('li.hit > span').text(post.hit);
		if(post.bd && feedBoard[post.bd]) el.find('li.board').text(boardType[post.bd]);
		else if(post.bd && feedBoardCm[post.bd]) el.find('li.board').text(feedBoardCm[post.bd]);
		if(post.cate) el.find('li.cate').text("["+post.cate+"]");
		if(post.date) el.find('li.date').attr("title",post.date).prettyDate();
		if(post.writer) {
			if(post.writer.search(/img\ssrc/i)) el.find('li.writer').html(post.writer.replace(/\ssrc='/," src='http://www.clien.net/"));
			else el.find('li.writer').text(post.writer);
		}
		el.attr('id',post.bd + "_" + post.idx);
		if($('.post_watch:last').length>0) $('.post_watch:last').after(el);
		else el.prependTo('#postContainer');

		if(reWords.length>0 && post.title ){
			var tmpTitle = post.title.toLowerCase();
			for(var i in reWords){
				if(tmpTitle.search(reWords[i])>=0) { createWordNoti({title:post.title,link:href}); break; }
			}
		}

		if($('div.post').length>300){
			for(var i = $('div.post').length;i>300;i--) $('div.post:nth-child('+i+')').remove();
		}
	} else {
		return false;
	}
	return true;
}

var updatePost = function(post) {
	if(!post.bd || !post.idx) return false;
	var el = $("#" + post.bd + "_" + post.idx);
	if(el) {
		if(post.title) {
			var aTag = el.find('div.post_title > a.title');
			aTag.html(post.title);
			var tmpTitle = post.title.toLowerCase();
			if(reWords.length>0){
				for(var i in reWords){
					if(tmpTitle.search(reWords[i])>0) { createWordNoti({title:post.title,link:aTag.attr('href')}); break; }
				}
			}
		}
		if(post.reps) el.find('li.reps > span').text(post.reps).show();
		if(post.hit) el.find('li.hit > span').text(post.hit);
		if(post.cate) el.find('li.cate').text("["+post.cate+"]");
	}
}

var updateNewCount = function(){
	var c = $('div.post_new').length;
	if(c>0) document.title = pageTitle + " (" + c + ")";
	else document.title = pageTitle;
}

var removeNewTag = function(el){
	$(el).animate({borderWidth:'1px'},500,function(){
		$(el).removeClass('post_new post_cm').css('border-width','');
		updateNewCount();
	});
}
var postMouseLeave = function(){
	if(!$(this).hasClass('post_watch')) $(this).find('.btn_watch').hide();
}
var postMouseEnter = function() {
	$(this).find('.btn_watch').show();
}

var togglePostWatch = function(){
	var post = $(this).parents('.post');
	if(!post.hasClass('post_watch')) {
		$(this).text('★');
		$('.post_top:first').removeClass('post_top');
		post.addClass('post_watch post_top').prependTo('#postContainer');
	} else {
		$(this).text('☆');
		$('.post:not(.post_watch):first').before(post);
		post.removeClass('post_watch');
		$('.post_top:first').removeClass('post_top');
		$('.post:first').addClass('post_top');
	}
	return false;
}

var addNoticeWord = function(word){
	$('#ulNotiWords li.noword').hide();
	var el = $('<li class="addWord"><div class="alert alert-info"><button type="button" class="close" data-dismiss="alert">×</button></div></li>');
	el.find('div').append($('<span class="notiword">'+word+'</span>'));
	el.appendTo('#ulNotiWords');
	el.find('.alert').bind('closed',function(){
		$(this).parent().remove();
		if($('#ulNotiWords > .addWord').length<3) $('#divInputAddWord').show();
		if($('#ulNotiWords > .addWord').length==0) $('#ulNotiWords li.noword').show();
	});
	if($('#ulNotiWords > .addWord').length>=3) $('#divInputAddWord').hide();
};


function createWordNoti(opt) {
	if(window.webkitNotifications && window.webkitNotifications.checkPermission() == 0 ){
		var noti = window.webkitNotifications.createNotification('http://cm.c4t.so/assets/img/noti.png',opt.title,opt.link);
		noti.show();
	}
}

function setCookie(c_name,value,exdays) {
	var exdate=new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
	document.cookie=c_name + "=" + c_value;
}

function getCookie(c_name) {
	var i,x,y,ARRcookies=document.cookie.split(";");
	for (i=0;i<ARRcookies.length;i++) {
		x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
		y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
		x=x.replace(/^\s+|\s+$/g,"");
		if (x==c_name) { return unescape(y); }
	}
	return false;
}

var updateFeedBoard = function() {
	var bd = getCookie('arrFeedBoard');
	if(bd){
		feedBoard = jQuery.parseJSON(bd);
		for(var i in feedBoard){$('#frmFeed > ul').find('input[value="'+i+'"]').attr('checked',true);}
	}else{
		$('#frmFeed > ul').find('input').attr('checked',true);
		setCookie('arrFeedBoard',JSON.stringify(feedBoard),1000);
	}

	var bd = getCookie('arrFeedBoardCm');
	if(bd) feedBoardCm = jQuery.parseJSON(bd);
}

var getCmBoard = function(){
	$('#frmFeedCm > ul').html('');
	$.getJSON('/getCmBoardType',function(cmType, stat){
		if(stat=='success' && cmType) {
			for(var i in cmType){
				var el = $(feedLiStr);
				el.find('input').val(i).after(cmType[i]);
				if(feedBoardCm[i]) el.find('input').attr('checked',true);
				$('#frmFeedCm > ul').append(el);
			}
		}
	});
}

var chkCmBoardOpen = function() {
	var cmBoardCount = $('#frmFeedCm > ul').find('input:checked').length;
	jug.unsubscribe('cliem_cm');
	if(cmBoardCount > 0) {
		jug.subscribe('cliem_cm', function(data){
			$('#span_sc').css('background-color','#F89406');
			if(data.t == 'u') {
				updatePost(data.p);
			} else if (data.t == 'c') {
				$('div.post_top:first').removeClass('post_top');
				prependPost(data.p);
				$("#" + data.p.bd + "_" + data.p.idx).addClass('post_new post_cm').css('background-color','#78AC10').show().animate({backgroundColor:$.Color({ alpha: 0 })},5000,function(){$(this).css('background-color','')});
				$('.post:first').addClass('post_top');
				updateNewCount();
			}
			$('#span_sc').animate({backgroundColor:$.Color({ alpha: 0 })}, {queue:false, duration:1000});
		});
	}
}

function openCompact() {
	window.open(document.URL, "compactMode","width=540,height=900,scrollbars=yes");
}

$(document).ready(function(){
	// load board type
	for(var i in boardType){
		var el = $(feedLiStr);
		el.find('input').val(i).after(boardType[i]);
		$('#frmFeed > ul').append(el);
		feedBoard[i] = true;
	}

	// load cookies
	var cWords = getCookie('arrWords');
	if(cWords){
		var tmp = jQuery.parseJSON(cWords);
		if(tmp.length > 0) reWords = tmp;
	}

	updateFeedBoard();
	getCmBoard();

	$('div.post').live('mouseenter',postMouseEnter).live('mouseleave',postMouseLeave);
	$('div.post_new').live('mouseover',function(){removeNewTag(this);});
	$('.btn_watch').live('click',togglePostWatch);
	
	$.post('/getCurrent'
		, { bd : getCookie('arrFeedBoard'), bdcm : getCookie('arrFeedBoardCm') }
		, function(posts){
			var postCount = posts.length - 1;
			for(var i in posts) {
				prependPost(posts[postCount-i]);
			}
			$('div.post').show();
		},'json').done(function(){
			jug.subscribe('cliem', function(data){
			$('#span_sc').css('background-color','#F89406');
			if(data.t == 'u') {
				updatePost(data.p);
			} else if (data.t == 'c') {
				$('div.post_top:first').removeClass('post_top');
				prependPost(data.p);
				$("#" + data.p.bd + "_" + data.p.idx).addClass('post_new').css('background-color','#FFD062').show().animate({backgroundColor:$.Color({ alpha: 0 })},5000,function(){$(this).css('background-color','')});
				$('.post:first').addClass('post_top');
				updateNewCount();
			} else if (data.t == 'uc'){
				$('#span_uc').text(data.msg);
			}
			$('#span_sc').animate({backgroundColor:$.Color({ alpha: 0 })}, {queue:false, duration:1000});
		});
		chkCmBoardOpen();
	});

	setInterval(function(){$('.post li.date').prettyDate();}, 10000);
	setInterval(function(){
		var i = parseInt($('#span_sc').attr('data-index'));
		$('#span_sc').text(indicator[i]).attr('data-index', (i+1)%indicator.length);
	},250);

	$('#addNoti').submit(function(){
		var word = $('#inputAddNotiWord').val().toLowerCase();
		if($.trim(word)!=""){ addNoticeWord($.trim(word)); }
		$('#inputAddNotiWord').val('').focus();
		return false;
	});

	$("#btnSaveMyNotice").click(function(){
		reWords = $.map($('.notiword'),function(el){return $.trim($(el).text());})
		setCookie('arrWords',JSON.stringify(reWords),1000);
	});

	$('#myNoti').bind('show',function(){
		$('li.addWord').remove();
		if(reWords.length>0){ for(var i in reWords){addNoticeWord(reWords[i]);} }
		else { $('#ulNotiWords li.noword').show(); }
		if(window.webkitNotifications && window.webkitNotifications.checkPermission() != 0 ){
			 window.webkitNotifications.requestPermission();
		}
	});

	$('#myFeed .btnSave').click(function(){
		var tmp = {};
		$('#frmFeed > ul').find('input:checkbox').each(function(){
			if($(this).attr('checked')){tmp[$(this).val()] = true;}
		});
		feedBoard = tmp;
		setCookie('arrFeedBoard',JSON.stringify(feedBoard),1000);

		$('#frmFeedCm > ul').find('input').each(function(){
			if($(this).attr('checked')) feedBoardCm[$(this).val()] = $(this).parent().text().replace(/^\d+\.\s*/,'');
			else feedBoardCm[$(this).val()] = false;
		});
		setCookie('arrFeedBoardCm',JSON.stringify(feedBoardCm),1000);
		chkCmBoardOpen();
	});

	$('#myFeed').bind('show',function(){
		$('#frmFeed > ul').find('input').each(function(){
			if(feedBoard[$(this).val()]) $(this).attr('checked',true);
			else $(this).attr('checked',false);
		});
		$('#frmFeedCm > ul').find('input').each(function(){
			if(feedBoardCm[$(this).val()]) $(this).attr('checked',true);
			else $(this).attr('checked',false);
		});
	});

	$('#myTheme .btnSave').click(function(){
		var tmp = $('#frmTheme > ul').find('input[name=optionsThemes]:checked').val();
		setCookie('myTheme',tmp,1000);
		window.location.reload();
	});

	$('#myTheme').bind('show',function(){
		var tmp = getCookie('myTheme');
		if(!tmp){ tmp = 'default'; }
		$('#frmTheme > ul').find('input[name=optionsThemes][value="'+tmp+'"]').attr('checked',true);
	});

	$('#myTodaysPark').bind('show',function(){
		$('#myTodaysPark > .modal-body > ul').html('');
		$.getJSON('/getTodaysPark',function(data){
			for(var i in data){
				var row = data[i];

				row.writer = row.writer.replace(/\ssrc='/," src='http://www.clien.net/");
				row.href = row.href.replace(/^\.\./,"http://www.clien.net/cs2");

				var el = $('<li></li>').html('<span class="writer">'+row.writer+'</span><a target="_blank" href="'+row.href+'">'+row.title+'</a>');
				$('#myTodaysPark > .modal-body > ul').append(el);
			}
		});
	});

	$('#div_scrollTop').click(function(){
		$('html, body').animate({
			scrollTop: '0px'
		},1500);
		return false;
	});

	$(window).scroll(function(){
		var scrollV = $(document).scrollTop();
		if(scrollV >= 500) {
			$('#div_scrollTop').show().css('top',scrollV + 20);
		}
		else $('#div_scrollTop').hide();
	});

});

